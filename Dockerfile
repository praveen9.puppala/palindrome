FROM gradle:6.6.1-jdk11 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM openjdk:8-jdk-alpine

COPY --from=build /home/gradle/src/build/libs/*.jar /demo-palindrome.jar
ENTRYPOINT ["java","-jar","/demo-palindrome.jar"]
CMD ["1234"]
